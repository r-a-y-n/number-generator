import java.io.FileWriter;
import java.io.IOException;

class Main {
    public static void main(String args[]) throws IOException {
        Randomizer randomizer = new Randomizer();
        int listLength = randomizer.getTotalNumCount();
        FileWriter fileWriter = new FileWriter("./test.output");

        for (int i = 0; i < listLength; i++) {
            int random = randomizer.getRandom();
            fileWriter.write(random + "\n");
            if (random == 20) {
                System.out.println(i);
            }
        }

        fileWriter.close();
    }
}