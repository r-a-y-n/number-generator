import java.util.HashMap;

class Randomizer {
    private HashMap<Integer, Integer> counts;
    private HashMap<Integer, Double> weights;
    private int totalNumCount = 997940;
    private int lastNum = 0;

    public Randomizer() {
        counts = new HashMap<Integer, Integer>();
        weights = new HashMap<Integer, Double>();

        for (int i = 0; i <= 20; i++) {
            counts.put(i, 0);
        }

        weights.put(0, 0.0);
        for (int i = 1; i <= 12; i++) {
            weights.put(i, 83000.0);
        }

        weights.put(13, 1000.0);
        weights.put(14, 500.0);
        weights.put(15, 250.0);
        weights.put(16, 100.0);
        weights.put(17, 50.0);
        weights.put(18, 25.0);
        weights.put(19, 10.0);
        weights.put(20, 5.0);
    }

    public int getTotalNumCount() {
        return totalNumCount;
    }

    public void setTotalNumCount(int newTotalNumCount) {
        this.totalNumCount = newTotalNumCount;
    }

    public int getRandom() {
        double rand = Math.random();
        double runningChanceTotal = 0;

        for (int i = 1; i <= 20; i++) {
            if (rand <= runningChanceTotal + prob(i) && rand > runningChanceTotal) {
                counts.put(i, counts.get(i) + 1);
                totalNumCount--;
                lastNum = i;
                return i;
            }

            runningChanceTotal += prob(i);
        }

        return 0;
    }

    private double prob(int number) {
        if (lastNum == number) {
            return 0;
        }

        double rawProb = (weights.get(number) - counts.get(number)) / totalNumCount;
        double rawLastNumProb = (weights.get(lastNum) - counts.get(lastNum)) / totalNumCount;

        return rawProb + rawProb * rawLastNumProb;
    }
}
